import * as gcp from '@pulumi/gcp';
import * as pulumi from '@pulumi/pulumi';

export function StaticImplements<T>(t: T) {return t;}

export type CallbackFunctionDefinition = gcp.storage.BucketEventHandler | gcp.storage.BucketEventCallbackFunctionArgs;
export type StringMap = pulumi.Input<{ [key: string]: pulumi.Input<string> }>
