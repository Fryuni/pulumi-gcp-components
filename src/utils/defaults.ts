function _defaults(a: any | undefined, b: any): any {
    // if (b === undefined) throw new Error('Default value cannot be undefined');
    if (a === undefined) return b;
    if (b && typeof b === 'object' && !Array.isArray(b)) {
        for (const k of Object.getOwnPropertyNames(b)) {
            if (b[k]) {
                a[k] = _defaults(a[k], b[k]);
            }
        }
    }
    return a;
}

export function defaults<T extends any>(a: T | undefined, ...b: T[]): T {
    let value: T = a as T;
    for (const d of b) {
        value = _defaults(value, d) as T;
    }
    return value;
}
