import { all, Input, Output, output } from '@pulumi/pulumi';

export class Singleton<T> {
    private instance!: T;
    private readonly builder: () => T;

    constructor(builder: () => T) {
        this.builder = builder;
    }

    public get(): T {
        if (this.instance === undefined) {
            this.instance = this.builder();
        }
        return this.instance;
    }
}

export class NamedSingleton<T> {
    private readonly instances: {
        [key: string]: T
    };
    private readonly builder: (name: string) => T;

    constructor(builder: (name: string) => T) {
        this.builder = builder;
        this.instances = {};
    }

    public get(name: Input<string>): Output<T> {
        return output(name)
            .apply(trueName => this._get(trueName));
    }

    public deepGet(...names: Input<string>[]): Output<any> {
        return all(names).apply(trueNames => {
            return this._deepGet(...trueNames);
        });
    }

    private _get(name: string): T {
        if (this.instances[name] === undefined) {
            this.instances[name] = this.builder(name);
        }
        return this.instances[name];
    }

    private _deepGet(...names: string[]): any {
        if (names.length === 0) return this;
        const nextLevel = this._get(names[0]);
        if (nextLevel instanceof NamedSingleton) {
            return nextLevel._deepGet(...names.slice(1));
        }
        return nextLevel;
    }
}
