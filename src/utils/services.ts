import { Service } from '@pulumi/gcp/projects';
import { Input, isUnknown, output, Output } from '@pulumi/pulumi';
import { HolderComponent } from '../holderComponent';
import { NamedSingleton, Singleton } from './singleton';

const servicesResource = new Singleton(
    () => new HolderComponent('projectServices'),
);
const undefinedProjectPlaceholder = '__rootProject';

const services2LayeredSingleton = new NamedSingleton<NamedSingleton<Service | undefined>>(
    project => new NamedSingleton<Service | undefined>(
        service => (isUnknown(project) || isUnknown(service)) ? undefined : new Service(
            `${project}_${service}`,
            {
                disableOnDestroy: false,
                project: project === undefinedProjectPlaceholder ? undefined : project,
                service,
            },
            {parent: servicesResource.get()},
        ),
    ),
);

export function projectService(
    project: Input<string | undefined>,
    service: Input<string>,
): Output<Service> {
    const projectId = output(project).apply(id => id ?? undefinedProjectPlaceholder);
    return services2LayeredSingleton.deepGet(projectId, service) as Output<Service>;
}
