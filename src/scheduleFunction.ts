import * as gcp from '@pulumi/gcp';
import { HttpCallbackFunctionArgs } from '@pulumi/gcp/cloudfunctions';
import * as pulumi from '@pulumi/pulumi';
import { output } from '@pulumi/pulumi';
import * as random from '@pulumi/random';
import { HolderComponent } from './holderComponent';
import { LoggerComponent } from './logger';
import { defaults, NamedSingleton, projectService, Singleton } from './utils';

const serviceAccountParent = new Singleton(
    () => new HolderComponent('schedule-function-service-accounts'),
);
const serviceAccountId = new Singleton(
    () => new random.RandomId(
        'schefule-function-service-accounts-suffix',
        {
            prefix: 'job-func-caller-',
            byteLength: 5,
        },
        {parent: serviceAccountParent.get()},
    ),
);
const serviceAccounts = new NamedSingleton<gcp.serviceaccount.Account>(
    project => new gcp.serviceaccount.Account(
        `${project}-scheduled-functions`,
        {
            displayName: 'Scheduled Functions runner',
            accountId: serviceAccountId.get().hex,
            project,
        },
        {
            parent: serviceAccountParent.get(),
            dependsOn: [
                projectService(project, 'iam.googleapis.com'),
            ],
        },
    ),
);

export class ScheduledFunction extends LoggerComponent {
    readonly function: gcp.cloudfunctions.HttpCallbackFunction;
    private readonly functionIamMember!: gcp.cloudfunctions.FunctionIamMember;
    private readonly schedule: gcp.cloudscheduler.Job;

    constructor(name: string, args: ScheduleFunctionArgs, opts?: pulumi.CustomResourceOptions) {
        super('custom:components:ScheduledFunction', name, args, opts);

        let project: pulumi.Input<string>;
        if (args.project === undefined) {
            project = output(gcp.organizations.getClientConfig({parent: this}))
                .apply(config => config.project);
        } else {
            project = args.project;
        }

        args.function.project = pulumi
            .all([args.function.project, project])
            .apply(([p, p2]) => defaults(p, p2));

        this.function = new gcp.cloudfunctions.HttpCallbackFunction(
            name,
            args.function,
            {
                parent: this,
                dependsOn: [
                    projectService(args.function.project, 'cloudfunctions.googleapis.com'),
                    projectService(args.function.project, 'cloudbuild.googleapis.com'),
                    projectService(args.function.project, 'storage-api.googleapis.com'),
                    projectService(args.function.project, 'iam.googleapis.com'),
                ],
            },
        );

        const serviceAccount = serviceAccounts.get(project);
        this.functionIamMember = new gcp.cloudfunctions.FunctionIamMember(name, {
            project,
            cloudFunction: this.function.function.name,
            role: 'roles/cloudfunctions.invoker',
            member: pulumi.interpolate`serviceAccount:${serviceAccount.email}`,
        }, {parent: this});

        this.schedule = new gcp.cloudscheduler.Job(name, {
            project,
            region: args.jobRegion,
            schedule: args.schedule,
            httpTarget: {
                uri: this.function.httpsTriggerUrl,
                headers: args.headers,
                httpMethod: args.httpMethod !== undefined ? args.httpMethod :
                    args.body === undefined ? 'GET' : 'POST',
                body: !!args.body
                    ? output(args.body)
                        .apply(body => Buffer.from(body, 'utf-8').toString('base64'))
                    : undefined,
                oidcToken: {
                    serviceAccountEmail: serviceAccount.email,
                },
            },
        }, {
            parent: this,
            dependsOn: [
                this.functionIamMember,
                projectService(project, 'cloudscheduler.googleapis.com'),
            ],
        });
    }
}

export interface ScheduleFunctionArgs {
    readonly schedule: pulumi.Input<string>
    readonly function: HttpCallbackFunctionArgs

    readonly project?: pulumi.Input<string>

    readonly jobRegion: pulumi.Input<string>
    readonly headers?: pulumi.Input<{
        [key: string]: pulumi.Input<string>;
    }>;
    readonly body?: pulumi.Input<string>;
    readonly httpMethod?: pulumi.Input<'GET' | 'POST' | 'PUT'>;
}
