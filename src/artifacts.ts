import * as gcp from '@pulumi/gcp';
import * as pulumi from '@pulumi/pulumi';
import { LoggerComponent } from './logger';
import { defaults, projectService, StringMap } from './utils';

const artifacts = new pulumi.Config('gcp-components:artifacts');

const configDefault: ArtifactStoreConfiguration = {
    persistent: artifacts.getObject<BucketConfig>('persistent'),
    ephemeral: artifacts.getObject<BucketConfig>('ephemeral'),
};

export class ArtifactStore extends LoggerComponent {
    public readonly persistent: gcp.storage.Bucket;
    public readonly ephemeral: gcp.storage.Bucket;

    public constructor(name: string, args?: ArtifactStoreConfiguration, opts?: pulumi.CustomResourceOptions) {
        super('custom:components:ArtifactStore', name, args, opts);
        args = defaults<ArtifactStoreConfiguration>(args, configDefault);

        const clientProject = pulumi
            .output(gcp.organizations.getClientConfig({parent: this}))
            .apply(client => client.project);

        this.persistent = new gcp.storage.Bucket(
            `${name}-persistent`,
            {
                forceDestroy: defaults(args.persistent?.forceDestroy, false),
                storageClass: defaults(args.persistent?.storageClass, 'multi_regional'),
                location: defaults(args.persistent?.location, 'us'),
                labels: args.labels,
            },
            {
                parent: this,
                dependsOn: projectService(clientProject, 'storage-api.googleapis.com'),
            },
        );

        this.ephemeral = new gcp.storage.Bucket(
            `${name}-ephemeral`,
            {
                forceDestroy: defaults(args.ephemeral?.forceDestroy, true),
                storageClass: pulumi.output(args.ephemeral?.storageClass ?? 'regional')
                    .apply(storageClass => storageClass.toUpperCase()),
                location: defaults(args.ephemeral?.location, 'us-east1'),
                labels: args.labels,
                lifecycleRules: [
                    {
                        action: {
                            type: 'Delete',
                        },
                        condition: {
                            age: defaults(args.ephemeralDurationDays, 5),
                        },
                    },
                ],
            },
            {
                parent: this,
                dependsOn: projectService(clientProject, 'storage-api.googleapis.com'),
            },
        );
    }
}

interface ArtifactStoreConfiguration {
    persistent?: BucketConfig
    ephemeral?: BucketConfig
    ephemeralDurationDays?: pulumi.Input<number>
    labels?: StringMap
}

interface BucketConfig {
    location?: pulumi.Input<string>
    forceDestroy?: pulumi.Input<boolean>
    storageClass?: pulumi.Input<string>
}
