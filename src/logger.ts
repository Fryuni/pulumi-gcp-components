import * as pulumi from '@pulumi/pulumi';

export class LoggerComponent extends pulumi.ComponentResource {
    protected logDebug(msg: string): Promise<void> {
        return pulumi.log.debug(msg, this);
    }

    protected logInfo(msg: string): Promise<void> {
        return pulumi.log.info(msg, this);
    }

    protected logWarning(msg: string): Promise<void> {
        return pulumi.log.warn(msg, this);
    }

    protected logError(msg: string): Promise<void> {
        return pulumi.log.error(msg, this);
    }
}
