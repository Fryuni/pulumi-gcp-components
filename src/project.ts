import * as gcp from '@pulumi/gcp';
import * as pulumi from '@pulumi/pulumi';
import { projectService } from './utils';

export interface Owner {
    type: 'user' | 'serviceAccount'
    email: string
}

export interface ProjectDefinitionArgs {
    // Display name of the project
    projectName: pulumi.Input<string>
    // Id of the project, recommended a `@pulumi/random.RandomId` with a prefix
    projectId: pulumi.Input<string>
    // Folder in organization structure to add project to. Optional
    folderId?: pulumi.Input<string>
    // Billing account to bind the project to
    billingAccount: pulumi.Input<string>

    // Default zone for resources created with the provider in the component
    providerZone?: pulumi.Input<string>
    // Default region for resources created with the provider in the component
    providerRegion?: pulumi.Input<string>

    // Additional owners of the project, besides the account creating it
    projectOwners: Owner[]
    // AppEngine location. Must be defined to enable any service that relies on AppEngine
    // under the hood, like Cloud Scheduler and Cloud Tasks
    appengineLocation?: pulumi.Input<string>
    // Location of Default Firebase resources. Must be set to use any Firebase related resource
    firebaseDefaultLocation?: pulumi.Input<string>
}

// Mapping for macro regions to their main regions
// Required for resources bound to AppEngine _and_ region like Cloud Scheduler
const AppEngineMacroRegionMapping: Record<string, string> = {
    'us-central': 'us-central1',
};

export class ProjectDefinition extends pulumi.ComponentResource {
    public readonly gcpProject: gcp.organizations.Project;
    // Direct access to gcp.Provider. Best used by just adding the component in the parent hierarchy
    public readonly provider: gcp.Provider;
    public readonly providerZone: pulumi.Output<string>;
    public readonly providerRegion: pulumi.Output<string>;
    public readonly schedulerRegion?: pulumi.Output<string>;
    public readonly projectOwners: gcp.projects.IAMMember[];

    constructor(
        name: string,
        args: ProjectDefinitionArgs,
        opts?: pulumi.ComponentResourceOptions,
    ) {
        const dependencies: pulumi.Resource[] = [];

        // Create the project
        const project = new gcp.organizations.Project(
            name,
            {
                name: args.projectName,
                projectId: args.projectId,
                folderId: args.folderId,
                billingAccount: args.billingAccount,
            },
        );
        dependencies.push(project);

        // Create a service account to manage it on a new provider
        // This prevents the need to enable all APIs on the Creators project
        const managementAccount = new gcp.serviceaccount.Account(
            `${name}-pulumi-management`,
            {
                project: project.projectId,
                accountId: 'pulumi-management',
                displayName: 'Account used by Pulumi provider to manage resources in this project',
            },
            {
                parent: project,
                deleteBeforeReplace: true,
            },
        );
        dependencies.push(managementAccount);

        // Add new account as owner
        const managementIAM = new gcp.projects.IAMMember(
            `${name}-pulumi-management`,
            {
                project: project.projectId,
                member: pulumi.interpolate`serviceAccount:${managementAccount.email}`,
                role: 'roles/owner',
            },
            {
                parent: project,
            },
        );
        dependencies.push(managementIAM);

        // Create a key to access the new account
        const managementCredentials = new gcp.serviceaccount.Key(
            `${name}-pulumi-management`,
            {
                serviceAccountId: managementAccount.name,
            },
            {
                parent: project,
                additionalSecretOutputs: ['privateKey'],
            },
        );
        dependencies.push(managementCredentials);

        // Apply default provider zone
        const providerZone = pulumi.output(args.providerZone ?? 'us-east1-b');
        // Infer provider region from zone if not given
        const providerRegion = !!args.providerRegion
            ? pulumi.output(args.providerRegion)
            : providerZone.apply(z => z.substring(0, z.length - 2));

        // Create a provider for the new project
        const provider = new gcp.Provider(
            `${name}-provider`,
            {
                project: project.projectId,
                // Service Account Keys are received in base64
                credentials: managementCredentials.privateKey
                    .apply((key: string) => Buffer.from(key, 'base64').toString('utf-8')),
                zone: providerZone,
                region: providerRegion,
            },
            {
                parent: project,
                dependsOn: [
                    managementCredentials,
                    managementIAM,
                    projectService(project.projectId, 'cloudresourcemanager.googleapis.com'),
                ],
            },
        );

        if (args.appengineLocation === undefined) {
            // Try to get manually configured AppEngine location if not given
            const appEngine = gcp.appengine.Application.get(
                `${name}-appengine`,
                project.projectId,
            );
            args.appengineLocation = appEngine.locationId;
        } else {
            // Create AppEngine Application with set location
            const projectAppEngine = new gcp.appengine.Application(
                `${name}-appengine`,
                {
                    locationId: args.appengineLocation,
                    project: project.projectId,
                },
                {
                    parent: project,
                    provider,
                    dependsOn: [
                        projectService(project.projectId, 'appengine.googleapis.com'),
                    ],
                },
            );
            dependencies.push(projectAppEngine);
        }

        // Create Firebase project if given a location
        if (args.firebaseDefaultLocation !== undefined) {
            const projectFirebase = new gcp.firebase.Project(
                `${name}-firebase-project`,
                {
                    project: project.projectId,
                },
                {
                    parent: project,
                    provider,
                    dependsOn: [
                        projectService(project.projectId, 'firebase.googleapis.com'),
                    ],
                },
            );
            dependencies.push(projectFirebase);
            dependencies.push(new gcp.firebase.ProjectLocation(
                `${name}-firebase-location`,
                {
                    locationId: args.firebaseDefaultLocation,
                    project: projectFirebase.project,
                },
                {
                    parent: project,
                    provider,
                    dependsOn: [
                        projectFirebase,
                    ],
                },
            ));
        }

        // MAGIC PART
        // Everything is created inside the definition of a component, but is a dependency of the component
        // coming before it on the resource graph.
        // This allows resources to just add the component as parent and be correctly placed on the graph
        // as well as inherit the correct provider, project and default zone/region
        super('custom:utils:projectDefinition', name, args, {
            ...opts,
            parent: project,
            provider,
            dependsOn: pulumi.output(opts?.dependsOn)
                .apply(givenDependency => [
                    ...(
                        givenDependency === undefined ? []
                            : Array.isArray(givenDependency)
                            ? givenDependency
                            : [givenDependency]
                    ),
                    ...dependencies,
                ]),
        });

        // Make relevant resources public
        this.gcpProject = project;
        this.provider = provider;
        this.providerZone = providerZone;
        this.providerRegion = providerRegion;
        this.schedulerRegion = args.appengineLocation === undefined ? undefined
            : pulumi.output(args.appengineLocation)
                .apply(r => AppEngineMacroRegionMapping[r] ?? r);

        // Add additional owners of the project
        this.projectOwners = args.projectOwners.map(
            owner => new gcp.projects.IAMMember(
                `${name}-owner-${owner.email}`,
                {
                    project: project.projectId,
                    member: `${owner.type}:${owner.email}`,
                    role: 'roles/owner',
                },
                {
                    parent: project,
                    provider,
                },
            ),
        );
    }
}
