import { ComponentResource, ComponentResourceOptions } from '@pulumi/pulumi';

export class HolderComponent extends ComponentResource {
    constructor(name: string, opts?: ComponentResourceOptions) {
        super('custom:utils:holderResource', name, {}, opts);
    }
}
